/*
 * reading_blocks.c
 * Testa a leitura de blocos (dados, ponteiros, e outros).
 */

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>

#include "../include/apidisk.h"
#include "../include/data.h"
#include "../include/block_io.h"

int main()
{
	t2fs_block *data;
	
	data = blockRead(0);
	printf("Dumping bloco de dados 0:\n");
	blockPrint(data);
	
	data = blockRead(1);
	printf("\n\nDumping bloco de dados 1:\n");
	blockPrint(data);
	
	return 0;
}
