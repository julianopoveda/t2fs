// printBuffer.c
// Imprime o conte�do de um buffer em hexadecimal

#include <stdio.h>
#include <stdlib.h>

int main()
{
	unsigned char buffer[256];
	int i;

	// Inicializando buffer
	for(i = 0; i < 256; i++) {
		buffer[i] = i;
	}

	for(i = 0; i < 256; i++) {
		if(i%16 == 0) putchar('\n');
		printf("%2x ", buffer[i]);
	}

	putchar('\n');

	for(i = 0; i < 256; i++) {
		if(i%16 == 0) putchar('\n');
		printf("%3d ", buffer[i]);
	}

	putchar('\n');

	return 0;
}
