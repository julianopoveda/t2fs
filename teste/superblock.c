/*
 * superblock.c
 * Lê o primeiro bloco do disco (superbloco) e o interpreta.
 * Testa funções apidisk.
 */

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>

#include "../include/apidisk.h"
#include "../include/data.h"
#include "../include/block_io.h"

// Apenas para compilar, no futuro não vai funcionar
extern void initT2fs();

int main()
{
	char buffer[256];
	int i;

	printf ("sizeof(T2FS_SUPERBLOCK): %d\n", sizeof(t2fs_sblock));

	memset(buffer, 1, SECTOR_SIZE);
	printf("1a leitura do disco\n");
	printSector(0);

	printf("2a leitura do disco\n");
	read_sector(0, buffer);
	printf("Imprimindo buffer via main():\n");
	for(i = 0; i < 256; i++) {
		if(i%16 == 0) putchar('\n');
		printf("%02x ", (unsigned char) buffer[i]);
	}
	putchar('\n');


	printf("3a leitura do disco\n");
	printSector(0);

	t2fs_sblock *superblock1 = (t2fs_sblock *) buffer;
	printSuperBlock(superblock1);
	
	//exit(0);

	printf("4a leitura do disco\n");	
	initT2fs();
	
	printf("SECTOR_SIZE: %d\nBLOCK_SIZE: %d\n", SECTOR_SIZE, BLOCK_SIZE);
	
	//t2fs_block *block = blockRead(0);
	//blockPrint(block);

	initT2fs();

	printf("5a leitura do disco\n");
	printSector(0);

	printf("\nAgora lendo o setor 1:\n\n");

	printSector(0);
	printSector(0);
	read_sector(0, buffer);
	printSector(0);
	read_sector(0, buffer);
	printSector(0);
	read_sector(0, buffer);
	printSector(0);
	read_sector(0, buffer);
	
	printf("\nZerando a variável buffer...\n");
	for(i = 0; i < SECTOR_SIZE; i++)
		buffer[i] = 0;
	
	printf("\nAgora lendo o setores de forma sequencial:\n\n");

	printSector(0);
	read_sector(0, buffer);
	printSector(1);
	read_sector(1, buffer);
	printSector(2);
	read_sector(2, buffer);
	printSector(3);
	read_sector(3, buffer);
	printSector(4);
	read_sector(4, buffer);
	
	printSector(0);
	//read_sector(0, buffer);
	printSector(1);
	//read_sector(1, buffer);
	printSector(2);
	//read_sector(2, buffer);
	printSector(3);
	//read_sector(3, buffer);
	printSector(4);
	//read_sector(4, buffer);

	return 0;
}
