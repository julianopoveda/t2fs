#ifndef __t2fs_block_io__
#define __t2fs_block_io__

/*
 * Variáveis globais declaradas em block_io.c
 */

extern int BLOCK_SIZE; //em setores


void printSuperBlock(t2fs_sblock *sb);
void printSector(unsigned int n);
t2fs_block *blockAlloc();
t2fs_block *blockRead(unsigned int n);
void blockPrint(t2fs_block *block);

#endif //__t2fs_block_io__