#ifndef __t2fs_data__
#define __t2fs_data__

#include <stdint.h>

// Sério, não podemos programar sem isso
typedef enum {
	true  = 1,
	false = 0
} bool;

typedef struct {
	uint32_t id;
	uint16_t version;
	uint16_t superBlockSize;
	uint32_t diskSize;
	uint32_t nofBlocks;
	uint32_t blockSize;
	uint32_t reserved[108/4];
	uint32_t bitMapReg[64/4];
	uint32_t rootDirReg[64/4];
} t2fs_sblock;

// O tamanho do bloco varia de disco pra disco,
// mas o tamanho do setor é fixo (SECTOR_SIZE).
// Eu gostaria muito de criar uma estrutura genérica para tratar blocos.
// Em um sistema de arquivos real, o tamanho do bloco lógico deve ser fixo,
// então esse tamanho pode ser uma constante no código. Aqui poderia ser feito
// isso, mas pra cada tipo de disco (com tamanho de bloco diferentes), o código
// teria que ser recompilado. Talvez a ideia do professor seja exatamente
// recompilar o programa para tamanhos de discos diferentes. Mais do que isso,
// minha ideia é fazer o código genérico, que se ajuste em runtime ao tamanho
// do bloco do disco.


// Os dados dentro do bloco serão endereçãdos por byte.
// Isso é, para acessar dados dentro do bloco o offset significa
// o n-ésimo byte do bloco.
typedef struct {
	uint8_t *data;
} t2fs_block;

#endif // __t2fs_data__