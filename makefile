
HEADERS = $(wildcard include/*.h)
SOURCES = $(wildcard src/*.c)

all: lib testes
	

lib: t2fs block_io apidisk

testes: lib superblock reading_blocks
	
superblock: teste/superblock.c $(HEADERS)
	gcc -Wall -ggdb teste/superblock.c bin/*.o -o bin/superblock

reading_blocks: teste/reading_blocks.c $(HEADERS)
	gcc -Wall -ggdb teste/reading_blocks.c bin/*.o -o bin/reading_blocks

#_superblock: teste/superblock.c $(HEADERS)
#	gcc -Wall -ggdb -c teste/superblock.c -o bin/superblock.o

block_io: src/block_io.c $(HEADERS)
	gcc -Wall -ggdb -c src/block_io.c -o bin/block_io.o

t2fs: src/t2fs.c $(HEADERS)
	gcc -Wall -ggdb -c src/t2fs.c -o bin/t2fs.o

apidisk: others/apidisk.o
	cp -n others/apidisk.o bin/apidisk.o
	cp -n others/apidisk.o lib/apidisk.o

#reading_blocks: _reading_blocks block_io t2fs apidisk
#	gcc -Wall -ggdb 

clean:
	rm -f bin/superblock bin/reading_blocks bin/*.o
