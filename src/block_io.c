/*
 * block_io.c
 * Rotinas de baixo nível para IO com blocos e setores.
 */

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>

#include "../include/apidisk.h"
#include "../include/data.h"
#include "../include/block_io.h"
#include "../include/global.h"

// Esta variável será uma constante do programa
// Contém a quantidade de setores em um bloco lógico.
int BLOCK_SIZE; //medido em setores


/*
 * Acho que não é necessário usar os casts para unsigned ...
 */
void printSuperBlock(t2fs_sblock *sb)
{
	printf("             ID: 0x%x", sb->id);
	printf("  (%c%c%c%c)\n", (char)*((char *)&sb->id+0), (char)*((char *)&sb->id+1), (char)*((char *)&sb->id+2), (char)*((char *)&sb->id+3));
	printf("        Version: 0x%04x\n", sb->version);
	printf(" SuperBlockSize: 0x%04x (%ud)\n", sb->superBlockSize, sb->superBlockSize);
	printf("       DiskSize: 0x%04x (%ud) in bytes\n", sb->diskSize, sb->diskSize);
	printf("      NofBlocks: 0x%04x (%ud)\n", sb->nofBlocks, sb->nofBlocks);
	printf("      BlockSize: 0x%04x (%ud) in bytes\n", sb->blockSize, sb->blockSize);
	//printf("reserved: %d\n", superblock->diskSize);
	//printf("bitMapReg: %d\n", superblock->diskSize);
	//printf(": %d\n", superblock->diskSize);
	//printf("diskSize: %d\n", superblock->diskSize);
}

/*
 * Imprime dump hexadecimal do n-ésimo setor do disco (256 bytes).
 */
void printSector(unsigned int n)
{
	int i, status;
	uint8_t buffer[SECTOR_SIZE];
	
	memset(buffer, 0, SECTOR_SIZE);

	status = read_sector(n, (char *) buffer);
	
	if(status != 0) {
		fprintf(stderr, "printSector: read_sector não leu com sucesso\n");
		exit(-1);
	}

	printf("disk sector: %d, sector size: %d", n, SECTOR_SIZE);

	for(i = 0; i < SECTOR_SIZE; i++) {
		if(i%16 == 0) putchar('\n');
		printf("%02x ", buffer[i]);
	}
	putchar('\n');
}

t2fs_block *blockAlloc()
{
	t2fs_block *block;
	block = (t2fs_block *) malloc(sizeof(t2fs_block));
	block->data = (uint8_t *) malloc(BLOCK_SIZE*SECTOR_SIZE);
	return block;
}

/*
 * Lê o bloco 'n'.
 * O cálculo do bloco 'n' não considera o superbloco.
 */
t2fs_block *blockRead(unsigned int n)
{
	int i, status;
	unsigned int offset; // número do setor do disco
	t2fs_block *block;
	
	initT2fs();
	
	block = blockAlloc();
	
	/*
	 * O bloco 'n' começa no n-ésimo setor (desconsiderando
	 * o primeiro setor que é o superbloco).
	 */
	offset = BLOCK_SIZE*n + 1;
	
	for(i = 0; i < BLOCK_SIZE; i++) {
		offset += SECTOR_SIZE*i;
		status = read_sector(offset, (char *) (block->data + SECTOR_SIZE*i));
		if(status != 0) {
			fprintf(stderr, "blockRead: read_sector retornou erro\n");
			exit(-1);
		}
	}
	
	return block;
}

void blockPrint(t2fs_block *block)
{
	int i, line;
	
	printf("block size: %d bytes\n", BLOCK_SIZE*SECTOR_SIZE);
	
	for(i = 0, line = 0; i < BLOCK_SIZE*SECTOR_SIZE; i++) {
		/*
		 * Lógica para imprimir um número de linha (a posição do
		 * primeiro byte da linha) a cada 16 bytes escritos e
		 * para imprimir um char de nova linha.
		 */
		if(i%16 == 0) {
			if(i != 0) {
				putchar('\n');
			}
			printf("%04d %04X ", line*16, line*16);
			line++;
		}
		/*
		 * Cada linha são impressos 16 bytes, coloca um espaço no meio da linha.
		 */
		if(i%8 == 0) {
			putchar(' ');
		}
		
		printf("%02x ", block->data[i]);
	}
	
	putchar('\n');
}