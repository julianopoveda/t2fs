/*
 * superblock.c
 * Lê o primeiro bloco do disco (superbloco) e o interpreta.
 */

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>

#include "../include/apidisk.h"
#include "../include/data.h"
#include "../include/block_io.h"


static bool isT2fsInitialized = false;
static t2fs_sblock *superblock;

void initT2fs()
{
	int status;
	char buffer[SECTOR_SIZE];
	
	if(!isT2fsInitialized) {
		printf("initT2fs: inicializando...\n");
		
		printf("initT2fs: tamanho do setor: %d bytes\n", SECTOR_SIZE);

		status = read_sector(0, buffer);
		
		if(status != 0) {
			fprintf(stderr, "initT2fs: read_sector não leu com sucesso\n");
			exit(-1);
		}

		printf("initT2fs: dumping setor 0:\n");
		printSector(0);

		superblock = (t2fs_sblock *) malloc(SECTOR_SIZE);
		memcpy(superblock, buffer, SECTOR_SIZE);

		printf("initT2fs: Superbloco lido do disco:\n");
		printSuperBlock(superblock);
		
		if(superblock->id != 0x53463254) { //T2FS em ASCII
			printf("initT2fs: arquivo de disco não parece ser uma partição T2FS válida\n");
			exit(-1);
		}
		
		printf("initT2fs: tamanho do superbloco: %d bytes\n", sizeof(t2fs_sblock));
		
		BLOCK_SIZE = superblock->blockSize/SECTOR_SIZE;
		printf("initT2fs: tamanho do bloco de dados: %d setores (%d bytes)\n", BLOCK_SIZE, BLOCK_SIZE*SECTOR_SIZE);
		
		isT2fsInitialized = true;
	}
}
